import React from 'react'
import { useState } from 'react'

import { Container, Arrow, Wrapper, Slide, ImageContainer, InfoContainer, Title, Desc, Button, Image  } from './styles/sliderstyle.js';
import ArrowLeftOutlinedIcon from '@mui/icons-material/ArrowLeftOutlined';
import ArrowRightOutlinedIcon from '@mui/icons-material/ArrowRightOutlined';
import { sliderItems } from '../data'

const Slider = () => {
	
	const [ slideIndex, setSlideIndex ] = useState(0);

	const handleClick = (direction) => {
		if(direction === "left") {
			setSlideIndex(slideIndex > 0 ? slideIndex -1 : 2);
		} else {
			setSlideIndex(slideIndex < 2 ? slideIndex +1 : 0);
		}
	};

	return (

		<Container> 

			<Arrow direction="left" onClick={()=>handleClick("left")}>
				<ArrowLeftOutlinedIcon/>
				
			</Arrow>

			<Wrapper slideIndex={slideIndex}>
				{sliderItems.map(item => (
				<Slide bg={item.bg} key={item.id}>
					<ImageContainer>
						<Image src={item.img}/>
						{/*<img src={item.img} height="100%" width="95%"/>*/}
					</ImageContainer>
					<InfoContainer>
						<Title>{item.title}</Title>
						<Desc>{item.desc}</Desc>
						<Button>SHOP NOW</Button>
					</InfoContainer>
				</Slide>
				))}
				
			</Wrapper>

			<Arrow direction="right" onClick={()=>handleClick("right")}>
				<ArrowRightOutlinedIcon/>
			</Arrow>

		</Container>

	)
}

export default Slider