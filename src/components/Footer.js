import React from 'react'
import { Container, Left, Center, Right, Logo, Desc, SocialContainer, SocialIcon, Title, List, ListItem, ContactItem, Payment } from './styles/footerstyle.js'
import FacebookIcon from '@mui/icons-material/Facebook';
import InstagramIcon from '@mui/icons-material/Instagram';
import TwitterIcon from '@mui/icons-material/Twitter';
import PinterestIcon from '@mui/icons-material/Pinterest';
import RoomIcon from '@mui/icons-material/Room';
import PhoneIcon from '@mui/icons-material/Phone';
import MailIcon from '@mui/icons-material/Mail';

const Footer = () => {
	return (
		<Container>
			<Left>
				<Logo>SHOP</Logo>
				<Desc>
					There are many variations of Lorem Ipsum available
				</Desc>

				<SocialContainer>

					<SocialIcon color="385999">
						<FacebookIcon />
					</SocialIcon>

					<SocialIcon color="E4405F">
						<InstagramIcon />
					</SocialIcon>

					<SocialIcon color="55ACEE">
						<TwitterIcon />
					</SocialIcon>

					<SocialIcon color="E60023">
						<PinterestIcon />
					</SocialIcon>

				</SocialContainer>

			</Left> 

			<Center>
				<Title>Useful Links</Title>
				<List>
					<ListItem>Home</ListItem>
					<ListItem>Cart</ListItem>
					<ListItem>Men's Fashion</ListItem>
					<ListItem>Women's Fashion</ListItem>
					<ListItem>Accessories</ListItem>
					<ListItem>My Account</ListItem>
					<ListItem>Order Tracking</ListItem>
					<ListItem>Wishlist</ListItem>
					<ListItem>Wishlist</ListItem>
					<ListItem>Terms</ListItem>
				</List>
			</Center>

			<Right>
				<Title>Contact</Title>
				<ContactItem>
					<RoomIcon style={{marginRight:"10px"}}/> Atrium, Iloilo City
				</ContactItem>

				<ContactItem>
					<PhoneIcon style={{marginRight:"10px"}}/> +639 1234 5678
				</ContactItem>

				<ContactItem>
					<MailIcon style={{marginRight:"10px"}}/> contact@shop.com
				</ContactItem>

				<Payment src="https://i.ibb.co/Qfvn4z6/payment.png"/>

			</Right>

		</Container>
	)
}

export default Footer