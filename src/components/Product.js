import React from 'react'
import { Container, Circle, Image, Info, Icon } from './styles/productstyle.js';
import ShoppingCartOutlinedIcon from '@mui/icons-material/ShoppingCartOutlined';
import SearchIcon from '@mui/icons-material/Search';
import FavoriteBorderIcon from '@mui/icons-material/FavoriteBorder';

const Product = ({item}) => {

	return (
		<Container>
			<Circle />

			<Image src={item.img} />

			<Info>

				<Icon>
					<ShoppingCartOutlinedIcon/>
				</Icon>

				<Icon>
					<SearchIcon/>
				</Icon>

				<Icon>
					<FavoriteBorderIcon/>
				</Icon>

			</Info>

		</Container>
	)
}

export default Product