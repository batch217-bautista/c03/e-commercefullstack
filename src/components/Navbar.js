import React from 'react'
import { Container, Wrapper, Left, Center, Right, Language, SearchContainer, Input, Logo, MenuItem  } from './styles/navbarstyle.js';
import SearchIcon from '@mui/icons-material/Search';
import Badge from '@mui/material/Badge';
import ShoppingCartOutlinedIcon from '@mui/icons-material/ShoppingCartOutlined';

// import styled from 'styled-components'

// const Container = styled.div`
// 	height: 60px;
// 	background-color: black;
// `;



const Navbar = () => {
	return (
		<Container>
			<Wrapper>
				<Left>
					<Language>EN</Language>
					<SearchContainer>
						<Input placeholder="Search"/>
						<SearchIcon style={{color:"gray", fontSize:16}}/>
					</SearchContainer>
				</Left>

				<Center><Logo>SHOP</Logo></Center>

				<Right>
					
					<MenuItem>REGISTER</MenuItem>
					<MenuItem>LOG-IN</MenuItem>
					<MenuItem>
						<Badge badgeContent={4} color="primary">
						    <ShoppingCartOutlinedIcon />
						</Badge>
					</MenuItem>
				</Right>

			</Wrapper>
		</Container>
	)
}

export default Navbar