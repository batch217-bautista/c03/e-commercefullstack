import React from 'react'
import { Container, Info, Image, Title, Button } from './styles/categoryitemstyle.js';


const CategoryItem = ({item}) => {
	return (
		<Container>
			<Image src={item.img}/>
			{/*<img src={item.img} width="100%" height="100%"/>*/}
			<Info>
				<Title>{item.title}</Title>
				<Button>SHOP NOW</Button>
			</Info>
		</Container>
	)
}

export default CategoryItem 