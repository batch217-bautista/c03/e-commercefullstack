import React from 'react'
import { Container, Wrapper, Title, Form, Input, Button, Link } from './styles/loginStyle.js';

const Login = () => {
	return(
		<Container>
			<Wrapper>
				<Title>CREATE AN ACCOUNT</Title>
				<Form>
					<Input placeholder="username" />
					<Input placeholder="password" />

					<Button>LOGIN</Button>
					<Link>FORGOT YOUR PASSWORD?</Link>
					<Link>CREATE A NEW ACCOUNT</Link>

				</Form>
			</Wrapper>
		</Container>
	)
}

export default Login