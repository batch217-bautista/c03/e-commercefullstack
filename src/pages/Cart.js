import React from 'react'
import { Container, Wrapper, Title, Top, Bottom, TopButton, TopTexts, TopText, Info, Summary, Product, ProductDetail, Image, Details, ProductName, ProductId, ProductColor, ProductSize, PriceDetail, ProductAmountContainer, ProductAmount, ProductPrice, Hr, SummaryTitle, SummaryItem, SummaryItemText, SummaryItemPrice, Button } from './styles/cartStyle.js'
import Announcement from "../components/Announcement";
import Footer from "../components/Footer";
import Navbar from "../components/Navbar";
import AddIcon from '@mui/icons-material/Add';
import RemoveIcon from '@mui/icons-material/Remove';

const Cart = () => {
	return(
		<Container>
			<Announcement/>
			<Navbar/>
				<Title>YOUR CART</Title>
				<Top>
					<TopButton>CONTINUE SHOPPING</TopButton>
						<TopTexts>
							<TopText>Shopping Bag(2)</TopText>
							<TopText>Your Wishlist(0)</TopText>
						</TopTexts>
					<TopButton type="filled">CHECK OUT NOW</TopButton>
				</Top>

				<Bottom>
					<Info>

						<Product>
							<ProductDetail>
								<Image src ="https://s3-us-west-2.amazonaws.com/tabs.web.media/b/5/b5e6/b5e6-square-1536.jpg"/>
								<Details>
									<ProductName><b>Product:</b> 48 Laws of Power</ProductName>
									<ProductId><b>Id:</b>0123</ProductId>
									<ProductColor color="orange"/>
									<ProductSize><b>Size:</b>37.5</ProductSize>
								</Details>
							</ProductDetail>
							<PriceDetail>
								<ProductAmountContainer>
									<AddIcon/>
										<ProductAmount></ProductAmount>
										<ProductPrice>$ 30</ProductPrice>
									<RemoveIcon/>
								</ProductAmountContainer>
							</PriceDetail>
						</Product>

						<Hr/>

						<Product>
							<ProductDetail>
								<Image src ="https://s3-us-west-2.amazonaws.com/tabs.web.media/b/5/b5e6/b5e6-square-1536.jpg"/>
								<Details>
									<ProductName><b>Product:</b> 48 Laws of Power</ProductName>
									<ProductId><b>Id:</b>0123</ProductId>
									<ProductColor color="orange"/>
									<ProductSize><b>Size:</b>37.5</ProductSize>
								</Details>
							</ProductDetail>
							<PriceDetail>
								<ProductAmountContainer>
									<AddIcon/>
										<ProductAmount></ProductAmount>
										<ProductPrice>$ 30</ProductPrice>
									<RemoveIcon/>
								</ProductAmountContainer>
							</PriceDetail>
						</Product>

					</Info>

					<Summary>

						<SummaryTitle>ORDER SUMMARY</SummaryTitle>
						<SummaryItem>
							<SummaryItemText>Subtotal</SummaryItemText>
							<SummaryItemPrice>$ 80</SummaryItemPrice>
						</SummaryItem>

						<SummaryItem>
							<SummaryItemText>Estimated Shipping</SummaryItemText>
							<SummaryItemPrice>$ 5.90</SummaryItemPrice>
						</SummaryItem>

						<SummaryItem>
							<SummaryItemText>Shipping Discount</SummaryItemText>
							<SummaryItemPrice>$ -5.90</SummaryItemPrice>
						</SummaryItem>

						<SummaryItem  type="total">
							<SummaryItemText>Total</SummaryItemText>
							<SummaryItemPrice>$ 80</SummaryItemPrice>
						</SummaryItem>

						<Button>CHECKOUT NOW!</Button>

					</Summary>

				</Bottom>

			<Wrapper>
				
			</Wrapper>

			<Footer/>
		</Container>
	)
}

export default Cart