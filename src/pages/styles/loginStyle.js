import styled from 'styled-components'

const Container = styled.div`
	width: 100vw;
	height: 100vh;
	background: linear-gradient(
		rgba(255,255,255,0.5),
		rgba(255,255,255,0.5)
		), 
		url("https://wonderfulengineering.com/wp-content/uploads/2016/01/Christmas-Wallpapers-2-1.jpg");
	background-size: cover;
	display: flex;
	align-items: center;
	justify-content: center;

`;

const Wrapper = styled.div`
	width: 40%;
	padding: 20px;
	background-color: white;
`;

const Title = styled.h1`
	font-size: 24px;
	font-weight: 300;
`;

const Form = styled.form`
	display: flex;
	flex-wrap: wrap;
`;

const Input = styled.input`
	flex: 1;
	min-width: 40%;
	margin: 10px 0px;
	padding: 10px;
`;


const Button = styled.button`
	width: 40%;
	border: none;
	padding: 15px 20px;
	background-color: teal;
	color: white;
	cursor: pointer;

`;

const Link = styled.a`
	margin: 5px 0px;
	font-size: 12px;
	text-decoration: underline;
	cursor: pointer;
`;

export { Container, Wrapper, Title, Form, Input, Button, Link };