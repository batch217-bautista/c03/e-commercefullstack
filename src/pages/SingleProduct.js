import React from 'react'
import { Container, Wrapper, ImgContainer, Image, InfoContainer, Title, Desc, Price, FilterContainer, Filter, FilterTitle, FilterColor, FilterSize, FilterSizeOption, AddContainer, AmountContainer, Amount, Button } from './styles/singleProductstyles.js';
import Navbar from '../components/Navbar'
import Announcement from '../components/Announcement'
import Products from '../components/Products'
import Newsletter from '../components/Newsletter'
import Footer from '../components/Footer'
import AddIcon from '@mui/icons-material/Add';
import RemoveIcon from '@mui/icons-material/Remove';


const SingleProduct = () => {
	return(

		<Container>
			<Announcement/>
			<Navbar/>
 
			{/*single product page goes here*/}
			<Wrapper>
				<ImgContainer> 
					<Image src="https://i.ibb.co/S6qMxwr/jean.jpg"/>
				</ImgContainer>
					<InfoContainer>
						<Title>Jumpsuit</Title>
						<Desc>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</Desc>
						<Price>$20</Price>
						<FilterContainer>

						<Filter>
							<FilterTitle>Color</FilterTitle>
							<FilterColor color="black"/>
							<FilterColor color="darkblue"/>
							<FilterColor color="gray"/>
						</Filter>

						<Filter>
							<FilterTitle>Size</FilterTitle>
							<FilterSize>
								<FilterSizeOption>XS</FilterSizeOption>
								<FilterSizeOption>S</FilterSizeOption>
								<FilterSizeOption>M</FilterSizeOption>
								<FilterSizeOption>L</FilterSizeOption>
								<FilterSizeOption>XL</FilterSizeOption>
							</FilterSize>
						</Filter>

					</FilterContainer>

					<AddContainer>
						<AmountContainer> 
							<RemoveIcon/>
							<Amount>1</Amount>
							<AddIcon/>
						</AmountContainer>

						<Button>Add To Cart</Button>

					</AddContainer>

					</InfoContainer> 


			</Wrapper>

			<Newsletter/>
			<Footer/>
		</Container>
	)
}

export default SingleProduct