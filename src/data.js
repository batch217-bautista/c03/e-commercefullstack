export const sliderItems = [

	{
		id: 1,
		img: "https://c4.wallpaperflare.com/wallpaper/440/591/695/christmas-cute-girl-gift-wallpaper-preview.jpg",
		title: "CHRISTMAS SALE",
		desc: "NEVER COMPROMISE THIS HOLIDAY SEASON! GET 30% OFF FOR NEW ARRIVALS",
		bg: "f5fafd"
	},

	{
		id: 2,
		img: "https://contenthub-static.grammarly.com/blog/wp-content/uploads/2017/12/Happy-New-Year.jpg",
		title: "NEW YEAR SALE",
		desc: "NEVER COMPROMISE THIS HOLIDAY SEASON! GET 30% OFF FOR NEW ARRIVALS",
		bg: "fcf1ed"
	},

	{
		id: 3,
		img: "https://2.bp.blogspot.com/-aRYdQ2gqDTc/WkCZ_EkgmMI/AAAAAAAABW4/Xckk4s5gRVQqDoO-M-1HAFpg8u1_xmBKQCLcBGAs/s1600/6953268-new-year-gift.jpg",
		title: "GIFT SHOP",
		desc: "NEVER COMPROMISE THIS HOLIDAY SEASON! GET 30% OFF FOR NEW ARRIVALS",
		bg: "fbf0f4"
	},
];


export const categories = [

	{
		id: 1,
		img: "https://martinvalen.com/5459/men-s-button-short-sleeve-muscle-fit-shirt-in-yellow.jpg",
		title: "Men's shirt"
	},

	{
		id: 2,
		img: "https://i.ytimg.com/vi/xT-AYXo-R_Y/maxresdefault.jpg",
		title: "Laptop"
	},

	{
		id: 3,
		img: "https://images.squarespace-cdn.com/content/v1/6021e191b0db996839eb103c/1612847139603-MGBTXJISZX7FXUO1ASKX/shutterstock_books+resized.jpg?format=1500w",
		title: "Book"
	},
];

export const popularProducts = [

	{
		id: 1,
		img: "https://cdn.shopify.com/s/files/1/0396/5005/products/IMG_2731.jpg?v=1594491069"	
	},

	{
		id: 2,
		img: "https://ae01.alicdn.com/kf/H35b6e6a48f2a4c6283ab2ec9a2e9a0e1p/Drop-shipping-3D-Printed-Knight-Medieval-Armor-Men-t-shirt-Knights-Templar-Harajuku-Fashion-Tee-shirt.jpg"
	},

	{
		id: 3,
		img: "https://i.redd.it/bzxq8p74uy251.jpg"
	},

	{
		id: 4,
		img: "https://preview.redd.it/sx8qw8rpf5331.jpg?auto=webp&s=e6431f5711842e86e792e28c79c3732384f4ca45"
	},

	{
		id: 5,
		img: "https://cdn.shopify.com/s/files/1/1709/6503/products/Beard-Care-Club-Original-Blend-Top-View-Pomade-BCC-GROOMING-COMPANY_1024x1024.png?v=1518214432"
	},

	{
		id: 6,
		img: "https://alexnld.com/wp-content/uploads/2020/12/202a52c6-21bb-4397-8de4-bdfeabe35d82.jpg"
	},

	{
		id: 7,
		img: "https://www.callformore23.co.uk/winwin/ebay/1600/bf055/bf055_g_N.jpg"
	},

	{
		id: 8,
		img: "https://gift-market.imgix.net/ZQfA3ZTcrI.jpg"
	},

	// {
	// 	id: 9,
	// 	img: "https://comparepowertools.co.uk/wp-content/uploads/2019/09/8710085.jpg"
	// },

	// {
	// 	id: 10,
	// 	img: "https://preview.redd.it/0pg8ku3ylpg31.jpg?auto=webp&s=90843fef2bfbf7401472c7137ad563423d742e30"
	// }

];